
var contactsMap;

DG.then(function () {
    contactsMap = DG.map('contactsMap', {
        center: [54.98, 82.89],
        zoom: 13
    });
    DG.marker([54.98, 82.89]).addTo(contactsMap);
});