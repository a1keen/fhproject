$(document).ready(function () {

    // mask
    $("input[type='tel']").inputmask({
        "mask":  "+7 (999) - 999 - 99 - 99",
        "autoUnmask": true
    });

    // svg
    svg4everybody();

    // menu button
    $('.burgerButton').click(function () {
        $('.overlay').fadeIn(300)
        $('.sideMenu').addClass('active')
    })
    $('.overlay, .sideMenu__close').click(function () {
        $('.overlay').fadeOut(300)
        $('.sideMenu').removeClass('active')
    })


    //
    if ($(window).width() < 992) {
        $('.header__form').remove();
    }


    // custom select
    $('.customSelect select').each(function () {
        var $this = $(this),
            numberOfOptions = $(this).children('option').length;
        $this.addClass('select-hidden');
        $this.wrap('<div class="select"></div>');
        $this.after('<div class="select-styled"></div>');
        var $styledSelect = $this.next('div.select-styled');
        if ($this.children('option').is('[selected]')) {
            if ($this.children('option[selected]').length > 1) {
                var selected = $this.children('option[selected]').length;
                $styledSelect.html('<span>' + 'Выбрано' + ' (' + selected + ')' + '</span>');
            } else {
                $styledSelect.html('<span>' + $this.children('option[selected]').eq(0).text() + '</span>');
            }
        } else {
            $styledSelect.html('<span class="select-placeholder">' + $this.children('option').eq(0).text() + '</span>');
        }
        var $list = $('<ul />', {
            'class': 'select-options'
        }).insertAfter($styledSelect);
        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                html: '<span>' + $this.children('option').eq(i).html() + '</span>',
                rel: $this.children('option').eq(i).val(),
                class: $this.children('option').eq(i).attr('selected')
            }).appendTo($list);
        }
        var $listItems = $list.children('li');
        $styledSelect.click(function (e) {
            e.stopPropagation();
            $('div.select-styled.active').not(this).each(function () {
                $(this).removeClass('active').next('ul.select-options').hide();
            });
            $(this).toggleClass('active').next('ul.select-options').toggle();
        });
        $listItems.click(function (e) {
            e.stopPropagation();
            if ($this.attr('multiple')) {
                if ($this.children('option').eq($(this).index()).attr('selected')) {
                    $this.children('option').eq($(this).index()).removeAttr('selected')
                    $(this).removeClass('selected')
                } else {
                    $this.children('option').eq($(this).index()).attr('selected', true);
                    $(this).addClass('selected')
                }
                var selected = $(this).parents('.select').find('option' + '[selected]').length;
                if (selected == 0) {
                    $styledSelect.html('<span class="select-placeholder">' + $this.children('option').eq(0).text() + '</span>');
                } else if (selected > 1) {
                    $styledSelect.html('<span>' + 'Выбрано' + ' (' + selected + ')' + '</span>');
                } else {
                    $styledSelect.html('<span>' + $this.children('option[selected]').text() + '</span>')
                }
            } else {
                $styledSelect.html('<span>' + $(this).text() + '</span>');
                $listItems.removeClass('selected')
                $(this).addClass('selected');

                $this.children('option').attr('selected', false);
                $this.children('option').eq($(this).index()).attr('selected', true);
            }
        });
        $(document).click(function () {
            $styledSelect.removeClass('active');
            $list.hide();
        });
    });


     // countdown
    var elem = $('#countdown');
    var knobResize = function () {
        var width = Math.floor((elem.width() - 45) / 4);
        elem.find('.knob').trigger('configure', { width: width, height: width });
    }
    $(window).resize(function () {
        knobResize();
    });
    var myDate = new Date();
    myDate.setDate(myDate.getDate() + 5);
    elem.find('#knob-countdown').countdown({
        until: myDate,
        format: 'DHMS',
        onTick: function (e) {
            var secs = e[6], mins = e[5], hr = e[4], ds = e[3];
            elem.find("#countdown-ds").val(ds).trigger("change");
            elem.find("#countdown-hr").val(hr).trigger("change");
            elem.find("#countdown-min").val(mins).trigger("change");
            elem.find("#countdown-sec").val(secs).trigger("change");
        }
    });
    $('.knob').knob({
        inputColor: '#000',
        font: 'bold 24px'
    });
    knobResize();


    // close fancybox
    $('[data-fancybox-close]').click(function () {
        parent.$.fancybox.close();
    })
    
    // Form middle

    $('.sellForm__button').click(function (Event) {
        pageFormSender(Event);
    });

    // Footer form request
    $('.searchRequest__formButton').click(function (Event) {
        pageFormSender(Event);
    })

    // thank fancybox
    $('[data-fancybox-thank]').click(function (Event) {
        pageFormSender(Event);

    })



    // reviewsSlider
    var reviewsSlider = $('.reviewsSlider__slider').owlCarousel({
      
        loop: true,
        margin: 30,
        dots: false,
        responsive: {
            1200: {
                items: 2
            },
            992: {
                items: 2
            },
            768: {
                items: 1
            },
            0: {
                items: 1
            }
        }
    })
    $('#reviewsSlider-prev').click(function () {
        reviewsSlider.trigger('prev.owl.carousel')
    })
    $('#reviewsSlider-next').click(function () {
        reviewsSlider.trigger('next.owl.carousel')
    })


    // .productSlider
    var owl1 = $('.productSlider').owlCarousel({
        dots: false,
        items: 1,
        loop: true,
        nav: true,
        navText: ["<img class='previewSlider__buttonIcon' src='/images/general/icon-arrow-left-white.png'>", "<img class='previewSlider__buttonIcon' src='/images/general/icon-arrow-right-white.png'>"]
    });


    // .productCarousel
    var owl2 = $('.productCarousel').owlCarousel({
        items: 3,
        loop: true,
        nav: false,
        dots: false,
        center: true,
        mouseDrag: false,
        touchDrag: false,
        margin: 10
    });


    owl1.on('change.owl.carousel', function (event) {
        if (event.namespace && event.property.name === 'position') {
            var target = event.relatedTarget.relative(event.property.value, true);
            owl2.owlCarousel('to', target, 300, true);
        }
    })


    // buyPrevSlider
    var buyPrevSlider = $('.previewSlider').owlCarousel({
        items: 1,
        loop: true,
        dots: false,
        nav: true,
        navText: ["<img class='previewSlider__buttonIcon' src='/images/general/icon-arrow-left-white.png'>", "<img class='previewSlider__buttonIcon' src='/images/general/icon-arrow-right-white.png'>"]
        
    })


});

function pageFormSender(Event) {
    Event.preventDefault();
    var formId = $(Event.target).data('id');

    $.ajax({
        url: "/api/v1/form",
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: $('form[data-id=' + formId + ']').serialize() + '&formID=' + formId,
        success: function (data) {
            
            if (formId != 'footer') {
                parent.$.fancybox.close();
            }

            if (data.success && !data.errors) {
                $.fancybox.open($("#modal-2"));
            }
            else {
                jQuery.each(data.errors, function (key, value) {
                    alert(value[0]);
                });
            }

            console.log(data);
        }
    });
}
