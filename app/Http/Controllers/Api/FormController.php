<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Mail\FormReply;
use Illuminate\Support\Facades\Mail;

class FormController extends Controller
{

    /**
     * Send form data to email.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();

        $validator = Validator::make($data, [
            'form' => 'required|string|min:10|max:50',
            'name' => 'bail|nullable|alpha|max:25',
            'phone' => 'required|numeric|digits:10',
            'recallme' => 'string',
            'sendemail' => 'string',
            'text' => 'string|max:250',
            'formID' => 'required|string|min:1|max:10',
        ]);


        if ($validator->fails())
            return response()->json([ 'errors' =>  $validator->errors() ]);
        
        $data['href'] = $request->server('HTTP_REFERER');

        Mail::to( setting('site.form_email_send') )->send(
            new FormReply($data)
        );

        return response()->json([ 'success' => 'Скоро с вами свяжутся, ожидайте.' ]);
    }
}
