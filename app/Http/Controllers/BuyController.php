<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BuyController extends Controller
{

    /**
     * Handle to get buy objects list
     */
    public function getAllDirtObjects()
    {
        return $this->getObjects('buydirt');
    }

    /**
     * Handle to get buy objects list
     */
    public function getAllTakeObjects()
    {
        return $this->getObjects('takeobj');
    }

    /**
     * Handle to get buy objects list
     */
    public function getAllObjects()
    {
        return $this->getObjects('buyobj');
    }

    /**
     * Route: Get all objects
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getObjects($type)
    {
        // Get all posts
        $objects = DB::table('objects')->where(['type' => $type])->orderBy('created_at', 'desc')->paginate(6);

        return view("buy", [
            'objects' => $objects,
        ]);
    }

    /**
     * Route: Get object by ID
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getObject($id)
    {   
        $getRow = DB::table('objects')->find($id);

        // If row not found in database, then return 404 page
        if (!$getRow) {
            return abort(404);
        }
        
        // return view page
        return view("object", [
            'data' => $getRow
        ]);
    }
}
