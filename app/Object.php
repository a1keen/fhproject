<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Object extends Model
{

    protected $table = 'objects';

    public function save(array $options = [])
    {
        if ($this->author_id == null) {
            $this->author_id = \Auth::user()->id;
        }
        parent::save();
    }

    public function scopeCurrentUser($query)
    {   
        if (Auth::user()->role->name == 'admin')
            return;
        else
            return $query->where('author_id', Auth::user()->id);
    }
}
