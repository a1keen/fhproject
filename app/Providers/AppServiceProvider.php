<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('customPrice', function ($price) {
            return "<?php echo number_format($price, 0, '',' '); ?>";
        });

        Blade::directive('getDesc', function ($desc) {
            return "<?php echo htmlspecialchars_decode($desc); ?>";
        });

        Schema::defaultStringLength(191);
    }
}
