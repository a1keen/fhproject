<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Index Page

Route::get('/', function () {
    return view('index');
});

// Buy Objects

Route::get('/buy_objects', 'BuyController@getAllObjects')->name('buyobj');

// Take Objects

Route::get('/take_objects', 'BuyController@getAllTakeObjects')->name('takeobj');

// Buy Dirts

Route::get('/buy_dirts', 'BuyController@getAllDirtObjects')->name('buydirt');

// Product Lists

Route::get('/obj/{id}', 'BuyController@getObject')->where('id', '[0-9]+');

// Sell

Route::get('/sell', function () {
    return view('sell');
});

// About

Route::get('/about', function () {
    return view('about');
});

// Contacts

Route::get('/contacts', function () {
    return view('contacts');
});

// Legal Support

Route::get('/legal-support', function () {
    return view('legal-support');
});

// Admin pages

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
