<!DOCTYPE html>
<html lang="ru-RU">
  <head>
    <meta charset="utf-8">
    <title>@yield('meta_title', setting('site.title'))</title>
	<meta name="description" content="@yield('meta_description', setting('site.description')) - {{ setting('site.title') }}">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE = edge">
    <![endif]-->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="keywords" content="">
    <link rel="stylesheet" href="{{ url('/') }}/fonts/ProximaNova/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.css"><!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script><![endif]-->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ setting('site.google_analytics_tracking_id') }}"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', '{{ setting('site.google_analytics_tracking_id') }}');
    </script>
  </head>
  <body class="sell-page">
    <div class="wrapper">
      <header class="header header--padding" style="background-image: url({{ url('/') }}/images/general/bg-header.png)">
        <div class="container">
          <div class="header__inner">
            <div class="header__nav"><a class="header__navItem" href="/admin"><img class="header__navIcon" src="{{ url('/') }}/images/general/employee.png" alt="">
                <div class="header__navText">Вход для сотрудников</div></a><a class="header__navItem" href=""><img class="header__navIcon" src="{{ url('/') }}/images/general/mail.png" alt="">
                <div class="header__navText">Почта</div></a><a class="header__navItem" href=""><img class="header__navIcon" src="{{ url('/') }}/images/general/crm.png" alt="">
                <div class="header__navText">CRM</div></a><a class="header__navItem" href=""><img class="header__navIcon" src="{{ url('/') }}/images/general/user.png" alt="">
                <div class="header__navText">Вход в личный кабинет</div></a><a class="header__navItem" href=""><img class="header__navIcon" src="{{ url('/') }}/images/general/consultation.png" alt="">
                <div class="header__navText">Онлайн-консультант</div></a><a class="header__navItem" href=""><img class="header__navIcon" src="{{ url('/') }}/images/general/favorites.png" alt="">
                <div class="header__navText">Избранное</div>
                <div class="header__navCounter"><span>0</span></div></a></div>
            <div class="header__body">
              <div class="logo header__logo"><img class="logo__icon" src="{{ url('/') }}/images/general/logo.png" alt="">
                <div class="logo__text">Мы помогаем жить там,<br>где Вам хочется</div>
              </div>
              <button class="burgerButton header__burgerButton">
                <svg class="icon icon-menu-button ">
                  <use xlink:href="{{ url('/') }}/images/svg/symbol/sprite.svg#menu-button"></use>
                </svg>
              </button>
              <div class="header__content">
                <div class="header__contentTop">
                  <div class="alert header__contentAlert">
                    <div class="alert__text">При покупке и продаже Вашей недвижимости юридическое сопровождение сделки в <b>ПОДАРОК</b></div>
                  </div>
                </div>
                <div class="header__contentBottom"><a class="header__contentPhone" href="tel: +79255550344">+7 (495) 788-89-14</a><a class="header__contentPhone" href="tel: +79255550344">+7 (925) 555-03-44</a>
                  <button class="button button--smoll header__contentButton" data-fancybox href="#modal-1">Заказать звонок</button>
                </div>
              </div>
            </div>
            <div class="header__services">
              <div class="header__servicesCol"><a class="servicesBox servicesBox--main" href="/"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-header-home.png" alt="">
                  <div class="servicesBox__title">Главная</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href="/sell"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-sale.png" alt="">
                  <div class="servicesBox__title">Продать</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href="/buy_objects"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-buy.png" alt="">
                  <div class="servicesBox__title">Купить</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href=""><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-rent.png" alt="">
                  <div class="servicesBox__title">Сдать</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href="/take_objects"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-to-rent.png" alt="">
                  <div class="servicesBox__title">Снять</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href=""><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-header-location.png" alt="">
                  <div class="servicesBox__title">Недвижимость в Московской области</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href=""><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-country-app.png" alt="">
                  <div class="servicesBox__title">Загородная недвижимость</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href="/buy_dirts"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-green.png" alt="">
                  <div class="servicesBox__title">Купить земельный участок</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href="/legal-support"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-header-escort.png" alt="">
                  <div class="servicesBox__title">Оформление сделок и юр. сопровождение</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href="/about"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-header-about.png" alt="">
                  <div class="servicesBox__title">О компании</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href="/contacts"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-header-contacts.png" alt="">
                  <div class="servicesBox__title">Контакты</div></a></div>
            </div>
          </div>
        </div>
      </header>
      <div class="content">
        <div class="container">
          <section class="sectionForm">
            <form class="sellForm" action="" style="background-image: url({{ url('/') }}/images/general/bg-search-form.png);">
              <div class="sellForm__row">
                <div class="sellForm__col">
                  <div class="sellForm__title">контактное лицо</div>
                  <input class="customInput sellForm__input" type="text" placeholder="Ваше Имя">
                </div>
                <div class="sellForm__col">
                  <div class="sellForm__title">телефон</div>
                  <input class="customInput sellForm__input" type="tel" placeholder="Ваш номер телефона">
                </div>
                <div class="sellForm__col">
                  <div class="sellCols">
                    <div class="sellCols__col">
                      <div class="sellForm__title">общая площадь</div>
                      <div class="fromTo">
                        <input class="fromTo__input" type="number" value="0">
                        <div class="fromTo__label">м<sup>2</sup></div>
                      </div>
                    </div>
                    <div class="sellCols__col">
                      <div class="sellForm__title">расположение</div>
                      <div class="customCheckbox sellForm__customCheckbox">
                        <input class="customCheckbox__input" type="radio" id="sellForm-checkbox-1" name="sellForm-checkbox">
                        <label class="customCheckbox__label" for="sellForm-checkbox-1">Москва</label>
                      </div>
                      <div class="customCheckbox sellForm__customCheckbox">
                        <input class="customCheckbox__input" type="radio" id="sellForm-checkbox-2" name="sellForm-checkbox">
                        <label class="customCheckbox__label" for="sellForm-checkbox-2">Подмосковье</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="sellForm__col">
                  <div class="sellForm__title">e-mail</div>
                  <input class="customInput sellForm__input" type="text" placeholder="Ваш e-mail">
                </div>
                <div class="sellForm__col">
                  <div class="sellForm__title">адрес</div>
                  <input class="customInput sellForm__input" type="text" placeholder="Адрес">
                </div>
                <div class="sellForm__col">
                  <div class="sellForm__title">контактное лицо</div>
                  <div class="choiceType">
                    <div class="choiceCheckbox choiceType__choiceCheckbox">
                      <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-1" name="sellForm-type">
                      <label class="choiceCheckbox__label" for="sellForm-type-1">Комната</label>
                    </div>
                    <div class="choiceCheckbox choiceType__choiceCheckbox">
                      <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-2" name="sellForm-type">
                      <label class="choiceCheckbox__label" for="sellForm-type-2">1</label>
                    </div>
                    <div class="choiceCheckbox choiceType__choiceCheckbox">
                      <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-3" name="sellForm-type">
                      <label class="choiceCheckbox__label" for="sellForm-type-3">2</label>
                    </div>
                    <div class="choiceCheckbox choiceType__choiceCheckbox">
                      <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-4" name="sellForm-type">
                      <label class="choiceCheckbox__label" for="sellForm-type-4">3</label>
                    </div>
                    <div class="choiceCheckbox choiceType__choiceCheckbox">
                      <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-5" name="sellForm-type">
                      <label class="choiceCheckbox__label" for="sellForm-type-5">4</label>
                    </div>
                    <div class="choiceCheckbox choiceType__choiceCheckbox">
                      <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-6" name="sellForm-type">
                      <label class="choiceCheckbox__label" for="sellForm-type-6">5</label>
                    </div>
                    <div class="choiceCheckbox choiceType__choiceCheckbox">
                      <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-7" name="sellForm-type">
                      <label class="choiceCheckbox__label" for="sellForm-type-7">< 5</label>
                    </div>
                    <div class="choiceCheckbox choiceType__choiceCheckbox">
                      <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-8" name="sellForm-type">
                      <label class="choiceCheckbox__label" for="sellForm-type-8">Доля в квартире</label>
                    </div>
                    <div class="choiceCheckbox choiceType__choiceCheckbox">
                      <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-9" name="sellForm-type">
                      <label class="choiceCheckbox__label" for="sellForm-type-9">Студия</label>
                    </div>
                  </div>
                </div>
                <div class="sellForm__col">
                  <div class="sellForm__title">желаемый офис</div>
                  <div class="customSelect">
                    <select name="">
                      <option value="БЕСТ на Проспекте Мира 1">БЕСТ на Проспекте Мира 1</option>
                      <option value="БЕСТ на Проспекте Мира 2">БЕСТ на Проспекте Мира 2</option>
                      <option value="БЕСТ на Проспекте Мира 3">БЕСТ на Проспекте Мира 3</option>
                      <option value="БЕСТ на Проспекте Мира 4">БЕСТ на Проспекте Мира 4</option>
                      <option value="БЕСТ на Проспекте Мира 5">БЕСТ на Проспекте Мира 5</option>
                      <option value="БЕСТ на Проспекте Мира 6">БЕСТ на Проспекте Мира 6</option>
                    </select>
                  </div>
                </div>
                <div class="sellForm__col">
                  <button class="button button--wide sellForm__button">отправить заявку</button>
                </div>
                <div class="sellForm__col">
                  <div class="sellForm__alert">Нажимая «Отправить заявку», вы даёте своё согласие на обработку персональных данных</div>
                </div>
              </div>
            </form>
          </section>
          <section class="breadCrumbs content__breadCrumbs"><a class="breadCrumbs__item" href=""><img class="breadCrumbs__icon" src="{{ url('/') }}/images/general/icon-house.png" alt=""><span class="breadCrumbs__text">Главная</span></a><a class="breadCrumbs__item" href=""><span class="breadCrumbs__text">Купля-продажа квартир в Москве и Московской области</span></a><a class="breadCrumbs__item" href=""><span class="breadCrumbs__text">Квартиру или комнату</span></a></section>
          <section class="section">
            <h2 class="section__title">ПРОДАЖА КОМНАТЫ ИЛИ КВАРТИРЫ В МОСКВЕ</h2>
            <article class="appGrid section__appGrid">
              <div class="appGrid__col">
                <div class="appAdvantages">
                  <div class="appAdvantages__icon"><img class="appAdvantages__image" src="{{ url('/') }}/images/general/icon-app-advantages-1.png" alt=""></div>
                  <div class="appAdvantages__text">Продажа по рыночной цене, без потери времени</div>
                </div>
              </div>
              <div class="appGrid__col">
                <div class="appAdvantages">
                  <div class="appAdvantages__icon"><img class="appAdvantages__image" src="{{ url('/') }}/images/general/icon-app-advantages-2.png" alt=""></div>
                  <div class="appAdvantages__text">Наша реклама ваших квартир везде</div>
                </div>
              </div>
              <div class="appGrid__col">
                <div class="appAdvantages">
                  <div class="appAdvantages__icon"><img class="appAdvantages__image" src="{{ url('/') }}/images/general/icon-app-advantages-3.png" alt=""></div>
                  <div class="appAdvantages__text">Офисы у метро, доступность – минуты!</div>
                </div>
              </div>
              <div class="appGrid__col">
                <div class="appAdvantages">
                  <div class="appAdvantages__icon"><img class="appAdvantages__image" src="{{ url('/') }}/images/general/icon-app-advantages-4.png" alt=""></div>
                  <div class="appAdvantages__text">У нас самый лояльный процент</div>
                </div>
              </div>
              <div class="appGrid__col">
                <div class="appAdvantages">
                  <div class="appAdvantages__icon"><img class="appAdvantages__image" src="{{ url('/') }}/images/general/icon-app-advantages-5.png" alt=""></div>
                  <div class="appAdvantages__text">Отсутствие рисков и безопасность сделки. Всё застраховано</div>
                </div>
              </div>
            </article>
            <p class="section__text">Продать квартиру в Москве стремятся многие. Такая потребность может быть связана с переездом в другой город или страну, стремлением разменять жилплощадь или поселиться в более просторной квартире, в частном доме. Вне зависимости от причины продажи важно одно: чтобы реализовать недвижимость самостоятельно, нужны основательные знания и опыт. Именно они помогут не прогадать с ценой на жилье и не стать жертвой мошенников. Чтобы избежать рисков и при этом продать недвижимость в Москве выгодно и быстро, следует обратиться в агентство «БЕСТ-Недвижимость» за компетентной помощью опытных юристов и риелторов. Мы будем поддерживать Вас на каждом из этапов заключения сделки – такое тесное взаимодействие обязательно приведет к успеху!</p>
            <h3>ЭТАПЫ СОТРУДНИЧЕСТВА ПРИ ПРОДАЖЕ ЖИЛЬЯ В МОСКВЕ</h3>
            <p class="section__text">Вы заключили договор с нашим агентством. Это значит, что мы будем представлять Ваши интересы на рынке и защищать их. Делегируя нам полномочия по реализации недвижимости, Вы сэкономите не только время, но и силы и при этом сможете продать жилье по выгодной цене. О результативности сотрудничества с опытными риелторами говорят и цифры: сейчас почти 90% сделок заключается именно с привлечением агентства.</p>
            <p class="section__text">Продажа недвижимости предполагает следующие этапы:</p>
            <p class="section__text"><b>Оценка.</b> Чтобы грамотно определить стоимость Вашего объекта, эксперту предстоит проанализировать множество параметров: инфраструктуру района, характеристики квартиры (площадь, количество комнат, качество ремонта, состояние инженерных систем и коммуникаций), вид из окна и др. При этом важно отталкиваться не от стоимости аналогичных объектов, заявленной в рекламе, а от цены реально проданных квартир.</p>
            <p class="section__text"><b>Продвижение предложения.</b> Чтобы продать квартиру через агентство в Москве срочно, но при этом дорого, нужно составить правильное объявление и разместить его на популярных ресурсах. Мы публикуем рекламные предложения в специализированных изданиях, на профессиональных каналах, используем также риелторские базы. Такой подход позволяет большему числу потенциальных покупателей увидеть Ваше объявление. Конечно, чтобы действительно заинтересовать их, нужны качественные фотографии, которые сделаны при хорошем освещении и с соблюдением законов композиции. И с этим блестяще справятся наши сотрудники, которые работают с профессиональной аппаратурой.</p>
            <p class="section__text"><b>Показ квартиры.</b> Прежде чем принять потенциального покупателя, продавцу нужно подготовить квартиру к продаже. Наши специалисты расскажут Вам, что нужно сделать в первую очередь: привести в порядок подъезд, лифт и лестничную клетку, выбросить из квартиры мусор, убрать массивные предметы мебели, позаботиться о том, чтобы ничего не препятствовало естественному освещению. При проведении презентации объекта риелторы расскажут покупателям о достоинствах квартиры. Они не станут акцентировать внимание на мелких дефектах. Оформление сделки. Вам нужно лишь предоставить агенту документы на недвижимость – все остальное мы сделаем самостоятельно. Специалисты грамотно составят договор купли-продажи квартиры, а также зарегистрируют сделку.</p>
            <h3>ПОЧЕМУ НАМ МОЖНО ДОВЕРЯТЬ?</h3>
            <p class="section__text">Агентство «БЕСТ-Недвижимость» работает уже 25 лет. В штате компании – более 400 агентов, квалификация и опыт которых позволяют справляться со сложными задачами. Наши специалисты успешно реализуют различные объекты – вне зависимости от того, хотите Вы продать первичное или вторичное жилье, недвижимость премиум- или комфорт-класса. Мы также работаем с объектами, которые нужно реализовать в сжатые сроки (и первичка, и вторичка).</p>
            <p class="section__text">И делаем это блестяще!</p>
            <p class="section__text">«БЕСТ-Недвижимость» комплексно подходит к оказанию услуг и преследует важнейшую цель – заключить безопасную и прозрачную сделку, которая не доставит Вам хлопот в будущем.</p>
            <p class="section__text">Деятельность «БЕСТ-Недвижимость» отмечена многочисленными наградами: «Золотая середина», «Профессиональное призвание», «Каисса» и другие. Кроме того, компания является членом Российской ассоциации риелторов и Международной Федерации профессионалов рынка недвижимости.</p>
            <h3>ПРЕИМУЩЕСТВА «БЕСТ-НЕДВИЖИМОСТЬ»</h3>
            <p class="section__text">Планируете продать квартиру через агента в Москве? Оцените преимущества сотрудничества с нами:</p>
            <p class="section__text"><b>Собственная база объявлений.</b> Потенциальный покупатель сможет ознакомиться с Вашим предложением прямо на нашем сайте. <br><b>Комплексный подход.</b> Мы возьмем на себя все обязательства по продаже жилья – от оценки его стоимости до проведения показов и переговоров с покупателями, а также сопровождения сделки купли-продажи.<br><b>Гарантии безопасности сделки.</b> Мы тщательно анализируем риски, внимательно проверяем документацию и осуществляем контроль всех финансовых операций. Вы можете быть уверены, что Ваши интересы под надежной защитой.<br><b>Прозрачная схема оплаты.</b> Стоимость услуг агентства прописана в договоре. <br>Чтобы уточнить детали или получить ответы на интересующие вопросы, свяжитесь с нами по телефону +7 (495) 125-43-35 или напишите онлайн-консультанту на сайте.</p>
          </section>
        </div>
      </div>
      <footer class="footer">
        <div class="container">
          <article class="searchRequest footer__searchRequest">
            <h2 class="searchRequest__title">заявка на подбор недвижимости</h2>
            <form class="searchRequest__form" data-id="footer">
              <input type="hidden" name="form" value="Заявка на подбор недвижимости">
              <div class="searchRequest__formCol">
                <input class="customInput searchRequest__formInput" name="name" type="text" placeholder="Ваше имя">
                <input class="customInput searchRequest__formInput" name="phone" type="tel" placeholder="Ваш телефон" required>
                <div class="customCheckbox searchRequest__formCheckbox">
                  <input class="customCheckbox__input" type="radio" id="searchRequest-checkbox-1" name="recallme" checked="">
                  <label class="customCheckbox__label" for="searchRequest-checkbox-1">Перезвонить мне</label>
                </div>
                <div class="customCheckbox searchRequest__formCheckbox">
                  <input class="customCheckbox__input" type="radio" id="searchRequest-checkbox-2" name="sendemail">
                  <label class="customCheckbox__label" for="searchRequest-checkbox-2">Отправить на почту</label>
                </div>
              </div>
              <div class="searchRequest__formCol">
                <textarea class="customTextarea searchRequest__formTextarea" name="text" placeholder="Введите текст Вашего сообщения" maxlength="250"></textarea>
              </div>
              <div class="searchRequest__formBottom">
                <div class="searchRequest__formAlert">Нажимая «Отправить», вы даёте своё согласие на обработку персональных данных.</div>
                <button class="button searchRequest__formButton" data-id="footer">отправить</button>
              </div>
            </form>
          </article>
          <div class="footer__inner">
            <nav class="footer__nav">
              <div class="footer__navItem"><a class="footer__navLink" href="">Главная</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Услуги</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">О компании</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Наш офис</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Купить</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Продать</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Сдать</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Снять</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Обменять</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Советы</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Карта сайта</a></div>
            </nav>
            <div class="footer__content">
              <div class="footer__contentLeft">
                <div class="footer__contentLeftWrapper">
                  <div class="footer__contentLocation">
                    <div class="footer__contentTitle">Московская область:</div>
                    <div class="footer__contentLocationWrapper">
                      <div class="footer__contentLocationCol"><a class="footer__contentLocationLink" href="">Раменское</a><a class="footer__contentLocationLink" href="">Жуковский</a></div>
                      <div class="footer__contentLocationCol"><a class="footer__contentLocationLink" href="">Люберцы</a><a class="footer__contentLocationLink" href="">Воскресенск</a></div>
                    </div>
                  </div>
                  <div class="footer__contentLocation">
                    <div class="footer__contentTitle">Москва:</div>
                    <div class="footer__contentLocationWrapper">
                      <div class="footer__contentLocationCol"><a class="footer__contentLocationLink" href="">Кузьминки</a><a class="footer__contentLocationLink" href="">Рязанский проспект</a><a class="footer__contentLocationLink" href="">Братиславская</a></div>
                      <div class="footer__contentLocationCol"><a class="footer__contentLocationLink" href="">Выхино</a><a class="footer__contentLocationLink" href="">Котельники</a></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="footer__contentRight">
                <div class="footer__contentSocial">
                  <div class="footer__contentSocialWrapper">
                    <div class="footer__contentTitle">мы в соц. сетях:</div>
                    <div class="footer__contentSocialLinks"><a class="footer__contentSocialLink" href=""><img class="footer__contentSocialLinkImg" src="{{ url('/') }}/images/general/icon-inst.png" alt=""></a><a class="footer__contentSocialLink" href=""><img class="footer__contentSocialLinkImg" src="{{ url('/') }}/images/general/icon-vk.png" alt=""></a></div>
                  </div>
                </div>
                <div class="footer__contentAddress">
                  <div class="footer__contentTitle">ОФИС «НЕДВИЖИМОСТЬ - профит» В раменском</div><a class="footer__contentAddressLink" href="">г. Раменское, ул. Вокзальная д. 4, офис 209</a>
                </div>
              </div>
            </div>
            <div class="footer__info">
              <div class="footer__infoItem footer__infoItem--start">
                <div class="footer__copyright">© 1997 - 2017 «ПРОФИТ - Недвижимость»</div>
              </div>
              <div class="footer__infoItem footer__infoItem--center"><a class="footer__policy" href="">Политика хранения и обработки персональных данных</a></div>
              <div class="footer__infoItem footer__infoItem--end"><a class="footer__mail" href="mailto: ramprofit.ru@yandex.ru"><img class="footer__mailIcon" src="{{ url('/') }}/images/general/mail.png" alt="">
                  <div class="footer__mailText">ramprofit.ru@yandex.ru</div></a></div>
            </div>
          </div>
        </div>
      </footer>
    </div>
<div class="modal" id="modal-1">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <div class="modal__title">Закажите обратный звонок, мы вам перезвоним</div>
          <form class="modalForm">
            <input class="modalForm__input" type="text" placeholder="Ваше имя">
            <input class="modalForm__input" type="tel" placeholder="Ваш телефон" required>
            <button class="button modalForm__button" data-fancybox-thank>Заказать звонок</button>
          </form>
        </div>
      </div>
    </div>
    <div class="modal" id="modal-2">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button><img class="modal__logo" src="{{ url('/') }}/images/general/logo-color.png" alt="">
          <div class="modal__title">Спасибо за заявку!</div>
          <div class="modal__text">Ваши данные были успешно отправлены,<br>мы свяжемся с Вами в рабочее время<br>пн-сб с 10 до 21</div>
        </div>
      </div>
    </div>
    <div class="modal" id="modal-3">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <div class="modal__title">Получите выгодное предложение уже сегодня</div>
          <form class="modalForm">
            <input class="modalForm__input" type="text" placeholder="Ваше имя">
            <input class="modalForm__input" type="tel" placeholder="Ваш телефон" required>
            <button class="button button--blue modalForm__button" data-fancybox-thank>Подобрать недвижимость</button>
          </form>
        </div>
      </div>
    </div>
    <div class="modal" id="modal-4">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <div class="modal__title">Запишитесь на просмотр в любое удобное время</div>
          <form class="modalForm">
            <input class="modalForm__input" type="text" placeholder="Ваше имя">
            <input class="modalForm__input" type="tel" placeholder="Ваш телефон" required>
            <button class="button modalForm__button" data-fancybox-thank>Записаться на просмотр</button>
          </form>
        </div>
      </div>
    </div>
    <div class="modal" id="modal-5">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <div class="modal__title">Закажите звонок специалиста, мы вам перезвоним</div>
          <form class="modalForm">
            <input class="modalForm__input" type="text" placeholder="Ваше имя">
            <input class="modalForm__input" type="tel" placeholder="Ваш телефон" required>
            <button class="button button--blue modalForm__button" data-fancybox-thank>Заказать звонок</button>
          </form>
        </div>
      </div>
    </div>
    <div class="modal" id="modal-6">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <div class="modal__title">оставьте заявку на покупку жилья, мы вам перезвоним</div>
          <form class="modalForm">
            <input class="modalForm__input" type="text" placeholder="Ваше имя">
            <input class="modalForm__input" type="tel" placeholder="Ваш телефон" required>
            <button class="button modalForm__button" data-fancybox-thank>Перейти в каталог</button>
          </form>
        </div>
      </div>
    </div>
    <div class="modal" id="modal-7">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <div class="modal__title">оставьте заявку на продажу жилья, мы вам перезвоним</div>
          <form class="modalForm">
            <input class="modalForm__input" type="text" placeholder="Ваше имя">
            <input class="modalForm__input" type="tel" placeholder="Ваш телефон" required>
            <button class="button modalForm__button" data-fancybox-thank>Оставить заявку</button>
          </form>
        </div>
      </div>
    </div>
    <div class="sideMenu">
      <div class="sideMenu__wrapper">
        <div class="sideMenu__head"><a class="sideMenu__logo" href=""><img class="sideMenu__logoIcon" src="{{ url('/') }}/images/general/logo-color.png" alt=""></a>
          <button class="sideMenu__close"><img class="sideMenu__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
        </div>
        <ul class="sideMenu__userNav">
          <li class="sideMenu__userNavItem"><a class="sideMenu__userNavLink" href="">
              <div class="sideMenu__userNavIcon"><img src="{{ url('/') }}/images/general/employee.png" alt=""></div><span>Вход для сотрудников</span></a></li>
          <li class="sideMenu__userNavItem"><a class="sideMenu__userNavLink" href="">
              <div class="sideMenu__userNavIcon"><img src="{{ url('/') }}/images/general/mail.png" alt=""></div><span>Почта</span></a></li>
          <li class="sideMenu__userNavItem"><a class="sideMenu__userNavLink" href="">
              <div class="sideMenu__userNavIcon"><img src="{{ url('/') }}/images/general/crm.png" alt=""></div><span>CRM</span></a></li>
          <li class="sideMenu__userNavItem"><a class="sideMenu__userNavLink" href="">
              <div class="sideMenu__userNavIcon"><img src="{{ url('/') }}/images/general/user.png" alt=""></div><span>Вход в личный кабинет</span></a></li>
          <li class="sideMenu__userNavItem"><a class="sideMenu__userNavLink" href="">
              <div class="sideMenu__userNavIcon"><img src="{{ url('/') }}/images/general/consultation.png" alt=""></div><span>Онлайн-консультант</span></a></li>
          <li class="sideMenu__userNavItem"><a class="sideMenu__userNavLink" href="">
              <div class="sideMenu__userNavIcon"><img src="{{ url('/') }}/images/general/favorites.png" alt=""></div><span>Избранное</span></a></li>
        </ul>
        <ul class="sideMenu__mainNav">
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon sideMenu__mainNavIcon--white"><img src="{{ url('/') }}/images/general/icon-header-home.png" alt=""></div><span>Главная</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-sale.png" alt=""></div><span>Продать</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-buy.png" alt=""></div><span>Купить</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-rent.png" alt=""></div><span>Сдать</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-to-rent.png" alt=""></div><span>Снять</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-header-location.png" alt=""></div><span>Недвижимость в Московской области</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-country-app.png" alt=""></div><span>Загородная недвижимость</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-green.png" alt=""></div><span>Купить земельный участок</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-header-escort.png" alt=""></div><span>Оформление сделок и юр. сопровождение</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-header-about.png" alt=""></div><span>О компании</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-header-contacts.png" alt=""></div><span>Контакты</span></a></li>
        </ul>
      </div>
    </div>
    <div class="modal modal--wide" id="modalFilter">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <form class="searchForm" action="" style="background-image: url({{ url('/') }}/images/general/bg-search-form.png);">
            <div class="searchForm__head">
              <div class="choiceCheckbox searchForm__choiceCheckbox">
                <input class="choiceCheckbox__input" type="checkbox" id="searchForm-choice-1" name="searchForm-choice">
                <label class="choiceCheckbox__label" for="searchForm-choice-1">раменское</label>
              </div>
              <div class="choiceCheckbox searchForm__choiceCheckbox">
                <input class="choiceCheckbox__input" type="checkbox" id="searchForm-choice-2" name="searchForm-choice">
                <label class="choiceCheckbox__label" for="searchForm-choice-2">жуковский</label>
              </div>
              <div class="choiceCheckbox searchForm__choiceCheckbox">
                <input class="choiceCheckbox__input" type="checkbox" id="searchForm-choice-3" name="searchForm-choice">
                <label class="choiceCheckbox__label" for="searchForm-choice-3">подмосковье</label>
              </div>
              <div class="choiceCheckbox searchForm__choiceCheckbox">
                <input class="choiceCheckbox__input" type="checkbox" id="searchForm-choice-4" name="searchForm-choice">
                <label class="choiceCheckbox__label" for="searchForm-choice-4">москва</label>
              </div>
            </div>
            <div class="searchForm__firstRow">
              <div class="searchForm__firstRowCol">
                <div class="searchForm__title">тип недвижимости</div>
                <div class="customCheckbox searchForm__customCheckbox">
                  <input class="customCheckbox__input" type="radio" id="searchform-checkbox-1" name="searchform-checkbox">
                  <label class="customCheckbox__label" for="searchform-checkbox-1">Вторичное</label>
                </div>
                <div class="customCheckbox searchForm__customCheckbox">
                  <input class="customCheckbox__input" type="radio" id="searchform-checkbox-2" name="searchform-checkbox">
                  <label class="customCheckbox__label" for="searchform-checkbox-2">Новостройка</label>
                </div>
              </div>
              <div class="searchForm__firstRowCol">
                <div class="searchForm__title">общая площадь</div>
                <div class="fromTo">
                  <input class="fromTo__input" type="number" value="0">
                  <div class="fromTo__line"></div>
                  <input class="fromTo__input" type="number" value="0">
                  <div class="fromTo__label">м<sup>2</sup></div>
                </div>
              </div>
              <div class="searchForm__firstRowCol">
                <div class="searchForm__title">цена</div>
                <div class="fromTo">
                  <input class="fromTo__input" type="number" value="0">
                  <div class="fromTo__line"></div>
                  <input class="fromTo__input" type="number" value="0">
                  <div class="fromTo__label">руб.</div>
                </div>
              </div>
              <div class="searchForm__firstRowCol">
                <div class="searchForm__title">тип здания</div>
                <div class="customSelect">
                  <select name="" multiple>
                    <option value="hide">Тип недвижимости</option>
                    <option value="Панельный 1">Панельный 1</option>
                    <option value="Панельный 2">Панельный 2</option>
                    <option value="Панельный 3">Панельный 3</option>
                    <option value="Панельный 4">Панельный 4</option>
                    <option value="Панельный 5">Панельный 5</option>
                    <option value="Панельный 6">Панельный 6</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="searchForm__secondRow">
              <div class="searchForm__secondRowCol">
                <div class="searchForm__title">количество комнат</div>
                <div class="choiceType">
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-1" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-1">Комната</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-2" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-2">1</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-3" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-3">2</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-4" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-4">3</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-5" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-5">4</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-6" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-6">5</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-7" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-7">< 5</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-8" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-8">Доля в квартире</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-9" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-9">Студия</label>
                  </div>
                </div>
              </div>
              <div class="searchForm__secondRowCol">
                <div class="searchForm__title">метро</div>
                <div class="customSelect">
                  <select name="" multiple>
                    <option value="hide">Метро</option>
                    <option value="Панельный 2">Метро 2</option>
                    <option value="Панельный 3">Метро 3</option>
                    <option value="Панельный 4">Метро 4</option>
                    <option value="Панельный 5">Метро 5</option>
                    <option value="Панельный 6">Метро 6</option>
                  </select>
                </div>
                <div class="searchForm__metroLink"><a class="formLink" href="">Выбрать на схеме метро</a></div>
              </div>
              <div class="searchForm__secondRowCol">
                <div class="searchForm__title">расстояние до метро</div>
                <div class="fromTo">
                  <div class="fromTo__label">от</div>
                  <input class="fromTo__input" type="number" value="0">
                  <div class="fromTo__label">мин.</div>
                  <div class="customSelect">
                    <select name="">
                      <option value="Панельный 1">Пешком</option>
                      <option value="Панельный 2">Пешком 2</option>
                      <option value="Панельный 3">Пешком 3</option>
                      <option value="Панельный 4">Пешком 4</option>
                      <option value="Панельный 5">Пешком 5</option>
                      <option value="Панельный 6">Пешком 6</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="searchForm__bottom"><a class="formLink searchForm__bottomLink" href="">Очистить условия поиска</a>
              <button class="button searchForm__button">Показать (213)</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="modal modal--wide" id="modalSell">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <form class="sellForm" action="" style="background-image: url({{ url('/') }}/images/general/bg-search-form.png);">
            <div class="sellForm__row">
              <div class="sellForm__col">
                <div class="sellForm__title">контактное лицо</div>
                <input class="customInput sellForm__input" type="text" placeholder="Ваше Имя">
              </div>
              <div class="sellForm__col">
                <div class="sellForm__title">телефон</div>
                <input class="customInput sellForm__input" type="tel" placeholder="Ваш номер телефона">
              </div>
              <div class="sellForm__col">
                <div class="sellCols">
                  <div class="sellCols__col">
                    <div class="sellForm__title">общая площадь</div>
                    <div class="fromTo">
                      <input class="fromTo__input" type="number" value="0">
                      <div class="fromTo__label">м<sup>2</sup></div>
                    </div>
                  </div>
                  <div class="sellCols__col">
                    <div class="sellForm__title">расположение</div>
                    <div class="customCheckbox sellForm__customCheckbox">
                      <input class="customCheckbox__input" type="radio" id="sellForm-checkbox-1" name="sellForm-checkbox">
                      <label class="customCheckbox__label" for="sellForm-checkbox-1">Москва</label>
                    </div>
                    <div class="customCheckbox sellForm__customCheckbox">
                      <input class="customCheckbox__input" type="radio" id="sellForm-checkbox-2" name="sellForm-checkbox">
                      <label class="customCheckbox__label" for="sellForm-checkbox-2">Подмосковье</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="sellForm__col">
                <div class="sellForm__title">e-mail</div>
                <input class="customInput sellForm__input" type="text" placeholder="Ваш e-mail">
              </div>
              <div class="sellForm__col">
                <div class="sellForm__title">адрес</div>
                <input class="customInput sellForm__input" type="text" placeholder="Адрес">
              </div>
              <div class="sellForm__col">
                <div class="sellForm__title">контактное лицо</div>
                <div class="choiceType">
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-1" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-1">Комната</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-2" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-2">1</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-3" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-3">2</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-4" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-4">3</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-5" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-5">4</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-6" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-6">5</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-7" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-7">< 5</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-8" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-8">Доля в квартире</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-9" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-9">Студия</label>
                  </div>
                </div>
              </div>
              <div class="sellForm__col">
                <div class="sellForm__title">желаемый офис</div>
                <div class="customSelect">
                  <select name="">
                    <option value="БЕСТ на Проспекте Мира 1">БЕСТ на Проспекте Мира 1</option>
                    <option value="БЕСТ на Проспекте Мира 2">БЕСТ на Проспекте Мира 2</option>
                    <option value="БЕСТ на Проспекте Мира 3">БЕСТ на Проспекте Мира 3</option>
                    <option value="БЕСТ на Проспекте Мира 4">БЕСТ на Проспекте Мира 4</option>
                    <option value="БЕСТ на Проспекте Мира 5">БЕСТ на Проспекте Мира 5</option>
                    <option value="БЕСТ на Проспекте Мира 6">БЕСТ на Проспекте Мира 6</option>
                  </select>
                </div>
              </div>
              <div class="sellForm__col">
                <button class="button button--wide sellForm__button">отправить заявку</button>
              </div>
              <div class="sellForm__col">
                <div class="sellForm__alert">Нажимая «Отправить заявку», вы даёте своё согласие на обработку персональных данных</div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="overlay"></div>
    <script src="{{ mix('js/bundle.min.js') }}"></script>
  </body>
</html>