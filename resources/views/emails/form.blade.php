<style type="text/css">
  body,
  html, 
  .body {
    background: #f3f3f3 !important;
  }
</style>
<spacer size="16"></spacer>
<container>
  <spacer size="16"></spacer>
  <row>
    <columns>
      <h1>Заявка от {{ date('d.m.Y H:i') }}.</h1>

      <spacer size="16"></spacer>

      <callout class="secondary">
        <row>
          <columns>
            <p>
              <strong>Страница с которой была оформлена заявка</strong><br/>
              <a href="{{ $data['href'] }}">{{ $data['href'] }}</a>
            </p>
            <p>
              <strong>Оглавление формы</strong><br/>
              {{ $data['form'] }}
            </p>
            <p>
              <strong>Имя пользователя</strong><br/>
              {{ ($data['name'] != '') ? $data['name'] : 'Не заполнено' }}
            </p>
            <p>
              <strong>Телефон</strong><br/>
              +7{{ $data['phone'] }}
            </p>
            @if (isset( $data['recallme'] ))
            <p>
              <strong>Требуется перезвонить</strong><br/>
              Да
            </p>
            @endif
            @if (isset( $data['sendemail'] ))
            <p>
              <strong>Требуется отправить ответ на почту</strong><br/>
              Да
            </p>
            @endif
            @if (isset( $data['text'] ))
            <p>
              <strong>Текст обращения</strong><br/>
              {{ $data['text'] }}
            </p>
            @endif
            @if (isset( $data['formID'] ))
            <p>
              <strong>Номер формы на странице</strong><br/>
              {{ $data['formID'] }}
            </p>            
            @endif
          </columns>
        </row>
      </callout>
    </columns>
  </row>
</container>
