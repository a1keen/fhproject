<!DOCTYPE html>
<html lang="ru-RU">
  <head>
    <meta charset="utf-8">
    <title>@yield('meta_title', setting('site.title'))</title>
	  <meta name="description" content="@yield('meta_description', setting('site.description')) - {{ setting('site.title') }}">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE = edge">
    <![endif]-->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="">
    <link rel="stylesheet" href="{{ url('/') }}/fonts/ProximaNova/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.css"><!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script><![endif]-->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ setting('site.google_analytics_tracking_id') }}"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', '{{ setting('site.google_analytics_tracking_id') }}');
    </script>
  </head>
  <body class="productCard-page">
    <div class="wrapper">
      <header class="header" style="background-image: url({{ url('/') }}/images/general/bg-header.png)">
        <div class="container">
          <div class="header__inner">
            <div class="header__nav"><a class="header__navItem" href="/admin"><img class="header__navIcon" src="{{ url('/') }}/images/general/employee.png" alt="">
                <div class="header__navText">Вход для сотрудников</div></a><a class="header__navItem" href=""><img class="header__navIcon" src="{{ url('/') }}/images/general/mail.png" alt="">
                <div class="header__navText">Почта</div></a><a class="header__navItem" href=""><img class="header__navIcon" src="{{ url('/') }}/images/general/crm.png" alt="">
                <div class="header__navText">CRM</div></a><a class="header__navItem" href=""><img class="header__navIcon" src="{{ url('/') }}/images/general/user.png" alt="">
                <div class="header__navText">Вход в личный кабинет</div></a><a class="header__navItem" href=""><img class="header__navIcon" src="{{ url('/') }}/images/general/consultation.png" alt="">
                <div class="header__navText">Онлайн-консультант</div></a><a class="header__navItem" href=""><img class="header__navIcon" src="{{ url('/') }}/images/general/favorites.png" alt="">
                <div class="header__navText">Избранное</div>
                <div class="header__navCounter"><span>0</span></div></a></div>
            <div class="header__body">
              <div class="logo header__logo"><img class="logo__icon" src="{{ url('/') }}/images/general/logo.png" alt="">
                <div class="logo__text">Мы помогаем жить там,<br>где Вам хочется</div>
              </div>
              <button class="burgerButton header__burgerButton">
                <svg class="icon icon-menu-button ">
                  <use xlink:href="{{ url('/') }}/images/svg/symbol/sprite.svg#menu-button"></use>
                </svg>
              </button>
              <div class="header__content">
                <div class="header__contentTop">
                  <div class="alert header__contentAlert">
                    <div class="alert__text">При покупке и продаже Вашей недвижимости юридическое сопровождение сделки в <b>ПОДАРОК</b></div>
                  </div>
                </div>
                <div class="header__contentBottom"><a class="header__contentPhone" href="tel: +79255550344">+7 (495) 788-89-14</a><a class="header__contentPhone" href="tel: +79255550344">+7 (925) 555-03-44</a>
                  <button class="button button--smoll header__contentButton" data-fancybox href="#modal-1">Заказать звонок</button>
                </div>
              </div>
            </div>
            <div class="header__services">
              <div class="header__servicesCol"><a class="servicesBox servicesBox--main" href="/"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-header-home.png" alt="">
                  <div class="servicesBox__title">Главная</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href="/sell"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-sale.png" alt="">
                  <div class="servicesBox__title">Продать</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href="/buy_objects"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-buy.png" alt="">
                  <div class="servicesBox__title">Купить</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href=""><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-rent.png" alt="">
                  <div class="servicesBox__title">Сдать</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href="/take_objects"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-to-rent.png" alt="">
                  <div class="servicesBox__title">Снять</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href=""><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-header-location.png" alt="">
                  <div class="servicesBox__title">Недвижимость в Московской области</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href=""><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-country-app.png" alt="">
                  <div class="servicesBox__title">Загородная недвижимость</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href="/buy_dirts"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-green.png" alt="">
                  <div class="servicesBox__title">Купить земельный участок</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href="/legal-support"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-header-escort.png" alt="">
                  <div class="servicesBox__title">Оформление сделок и юр. сопровождение</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href="/about"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-header-about.png" alt="">
                  <div class="servicesBox__title">О компании</div></a></div>
              <div class="header__servicesCol"><a class="servicesBox" href="/contacts"><img class="servicesBox__icon" src="{{ url('/') }}/images/general/icon-header-contacts.png" alt="">
                  <div class="servicesBox__title">Контакты</div></a></div>
            </div>
          </div>
        </div>
      </header>
      <div class="content">
        <div class="container">
          <section class="breadCrumbs content__breadCrumbs"><a class="breadCrumbs__item" href=""><img class="breadCrumbs__icon" src="{{ url('/') }}/images/general/icon-house.png" alt=""><span class="breadCrumbs__text">Главная</span></a><a class="breadCrumbs__item" href="/buy"><span class="breadCrumbs__text">Каталог</span></a><a class="breadCrumbs__item" href=""><span class="breadCrumbs__text">{{ $data->address }}</span></a></section>
          <section class="section content__section">
            <h2 class="section__title">{{ $data->address }}</h2>
            <div class="productCard section__productCard">
              <div class="productCard__left">
                <div class="productSlider productCard__slider owl-carousel">
                  @foreach ( json_decode($data->images) as $image)
		                <a class="productSlider__item" data-fancybox href="{{ url('/') }}/storage/{{ $image }}">
                        <img class="productSlider__img" src="{{ url('/') }}/storage/{{ $image }}" alt="">
                    </a>
		              @endforeach
                </div>
                <div class="productCard__carousel">
                  <div class="productCarousel owl-carousel">
                  @foreach ( json_decode($data->images) as $image)
		                <div class="productCarousel__item"><img class="productCarousel__img" src="{{ url('/') }}/storage/{{ $image }}" alt=""></div>
                  @endforeach
                  </div>
                </div>
              </div>
              <div class="productCard__right">
                <div class="productInfo productCard__productInfo">
                  <div class="productInfo__head">
                    <div class="productInfo__id"><span class="productInfo__idText">ID объекта:</span><span class="productInfo__idVal">{{ $data->id }}</span></div><a class="productInfo__favorite" href=""><img class="productInfo__favoriteIcon" src="{{ url('/') }}/images/general/icon-product-favorite.png" alt=""><span class="productInfo__favoriteText">Добавить в избранное</span></a>
                  </div>
                  <div class="productInfo__pricing">    
                    <div class="productInfo__pricingCol">
                      <div class="productInfo__priceBig"><span class="productInfo__priceBigVal">@customPrice($data->total_price) </span><span class="productInfo__priceBigLabel">руб.</span></div>
                      <div class="productInfo__priceSmoll"><span class="productInfo__priceSmollVal">@customPrice($data->metr_price) </span><span class="productInfo__priceSmollLabel">руб. за м<sup>2</sup></span></div>
                    </div>
                    <div class="productInfo__pricingCol">
                      <div class="productInfo__price"><span class="productInfo__priceVal">2 440</span><span class="productInfo__priceLabel">$</span></div>
                      <div class="productInfo__price"><span class="productInfo__priceVal">2 106</span><span class="productInfo__priceLabel">€</span></div>
                    </div>
                  </div>
                  <div class="productInfo__sizes">
                    <ul class="productInfo__list">
                      <li class="productInfo__info"><span class="productInfo__infoText">Количество комнат:</span><span class="productInfo__infoVal">{{ $data->rooms }}</span></li>
                      <li class="productInfo__info"><span class="productInfo__infoText">Общая площадь:</span><span class="productInfo__infoVal">{{ $data->living_space }} м<sup>2</sup></span></li>
                    </ul>
                    <ul class="productInfo__list">
                      <li class="productInfo__info"><span class="productInfo__infoText">Жилая площадь:</span><span class="productInfo__infoVal">{{ $data->total_space }} м<sup>2</sup></span></li>
                      <li class="productInfo__info"><span class="productInfo__infoText">Площадь кухни:</span><span class="productInfo__infoVal">{{ $data->kitchen_space }} м<sup>2</sup></span></li>
                    </ul>
                  </div>
                  <ul class="productInfo__list">
                    <li class="productInfo__info"><b>Доля в квартире</b></li>
                  </ul>
                  <ul class="productInfo__list">
                    <li class="productInfo__info"><span class="productInfo__infoText">Материал стен дома :</span><span class="productInfo__infoVal">{{ $data->house_material }}</span></li>
                    <li class="productInfo__info"><span class="productInfo__infoText">Кол-во этажей :</span><span class="productInfo__infoVal">{{ $data->floors }}</span></li>
                    <li class="productInfo__info"><span class="productInfo__infoText">Лифт:</span><span class="productInfo__infoVal">@if ($data->lift == 'YES') Есть @else Нету @endif</span></li>
                    <li class="productInfo__info"><span class="productInfo__infoText">Этаж:</span><span class="productInfo__infoVal">{{ $data->floor }}</span></li>
                    <li class="productInfo__info"><span class="productInfo__infoText">Куда выходят окна:</span><span class="productInfo__infoVal">{{ $data->view_from_window }}</span></li>
                    <li class="productInfo__info"><span class="productInfo__infoText">Наличие балкона или лоджии:</span><span class="productInfo__infoVal">@if ($data->balcony == 'YES') Да @else Нет @endif</span></li>
                  </ul>
                </div>
                <div class="productContacts productCard__productContacts">
                  <div class="productContacts__head">
                    <div class="productContacts__headLeft">
                      <div class="productContacts__label">Агент</div><a class="productContacts__link" href="tel:+79167776298"><img class="productContacts__linkIcon" src="{{ url('/') }}/images/general/icon-product-phone.png" alt="">
                        <div class="productContacts__linkText">+7 (916) 777-62-98</div></a>
                    </div>
                    <button class="button button--smoll productContacts__button" data-fancybox href="#modal-1">Заказать звонок</button>
                  </div>
                  <div class="productContacts__label">Офис</div>
                  <div class="productContacts__label productContacts__label--normal">«НЕДВИЖИМОСТЬ - профит» в Раменском</div><a class="productContacts__link" href="tel:+74997098918"><img class="productContacts__linkIcon" src="{{ url('/') }}/images/general/icon-product-phone.png" alt="">
                    <div class="productContacts__linkText">+7 (499) 709-89-18</div></a><a class="productContacts__link productContacts__link--smoll" href=""><img class="productContacts__linkIcon" src="{{ url('/') }}/images/general/icon-product-location.png" alt="">
                    <div class="productContacts__linkText">129110, г. Раменское ул. Вокзальная. Дом 4 офис 209</div></a>
                </div>
              </div>
            </div>
            <p class="section__text">@getDesc($data->description)</p>
            <p class="section__text"> <b>Эксперт: </b>Дедов Евгений Иванович (+7-916-777-62-98). Лот N 53770.</p>
          </section>
          <section class="searchCapture content__searchCapture">
            <div class="searchCapture__map" id="searchMap"></div>
            <div class="searchCapture__info">
              <div class="searchCapture__choice">
                <div class="searchCapture__choiceText">Посмотреть другие объекты в радиусе</div>
                <input class="searchCapture__choiceInp" type="text" value="1">
                <div class="searchCapture__choiceLabel">км</div>
              </div>
              <button class="button button--smoll searchCapture__button" data-fancybox href="#modal-4">Записаться на просмотр</button>
            </div>
          </section>
          <section class="section content__section">
            <h2 class="section__title">Недавно просмотренные</h2>
            <div class="grid">
              <div class="grid__col"><a class="objectPreview" href="">
                  <div class="objectPreview__preview"><img class="objectPreview__previewImage" src="{{ url('/') }}/images/content/object.png" alt=""><img class="objectPreview__previewMagnifier" src="{{ url('/') }}/images/general/magnifier.png" alt=""></div>
                  <div class="objectPreview__info objectPreview__info--blue" href="">
                    <div class="objectPreview__content">
                      <div class="objectPreview__contentTitle">5-к квартира</div>
                      <div class="objectPreview__contentText">Москва, Лобачевского Улица, 92к4 <br>Проспект Вернадского</div>
                      <div class="objectPreview__contentText">15 минут пешком</div>
                    </div>
                    <div class="objectPreview__price"><span class="objectPreview__priceVal">52 000 000 </span><span class="objectPreview__priceLabel">руб.</span></div>
                  </div></a>
              </div>
              <div class="grid__col"><a class="objectPreview" href="">
                  <div class="objectPreview__preview"><img class="objectPreview__previewImage" src="{{ url('/') }}/images/content/object.png" alt=""><img class="objectPreview__previewMagnifier" src="{{ url('/') }}/images/general/magnifier.png" alt=""></div>
                  <div class="objectPreview__info objectPreview__info--aqua" href="">
                    <div class="objectPreview__content">
                      <div class="objectPreview__contentTitle">5-к квартира</div>
                      <div class="objectPreview__contentText">Москва, Лобачевского Улица, 92к4 <br>Проспект Вернадского</div>
                      <div class="objectPreview__contentText">15 минут пешком</div>
                    </div>
                    <div class="objectPreview__price"><span class="objectPreview__priceVal">52 000 000 </span><span class="objectPreview__priceLabel">руб.</span></div>
                  </div></a>
              </div>
              <div class="grid__col"><a class="objectPreview" href="">
                  <div class="objectPreview__preview"><img class="objectPreview__previewImage" src="{{ url('/') }}/images/content/object.png" alt=""><img class="objectPreview__previewMagnifier" src="{{ url('/') }}/images/general/magnifier.png" alt=""></div>
                  <div class="objectPreview__info objectPreview__info--yellow" href="">
                    <div class="objectPreview__content">
                      <div class="objectPreview__contentTitle">5-к квартира</div>
                      <div class="objectPreview__contentText">Москва, Лобачевского Улица, 92к4 <br>Проспект Вернадского</div>
                      <div class="objectPreview__contentText">15 минут пешком</div>
                    </div>
                    <div class="objectPreview__price"><span class="objectPreview__priceVal">52 000 000 </span><span class="objectPreview__priceLabel">руб.</span></div>
                  </div></a>
              </div>
            </div>
          </section>
          <section class="section content__section">
            <h2 class="section__title">Новые предложения</h2>
            <div class="grid">
              <div class="grid__col"><a class="objectPreview" href="">
                  <div class="objectPreview__preview"><img class="objectPreview__previewImage" src="{{ url('/') }}/images/content/object.png" alt=""><img class="objectPreview__previewMagnifier" src="{{ url('/') }}/images/general/magnifier.png" alt=""></div>
                  <div class="objectPreview__info objectPreview__info--blue" href="">
                    <div class="objectPreview__content">
                      <div class="objectPreview__contentTitle">5-к квартира</div>
                      <div class="objectPreview__contentText">Москва, Лобачевского Улица, 92к4 <br>Проспект Вернадского</div>
                      <div class="objectPreview__contentText">15 минут пешком</div>
                    </div>
                    <div class="objectPreview__price"><span class="objectPreview__priceVal">52 000 000 </span><span class="objectPreview__priceLabel">руб.</span></div>
                  </div></a>
              </div>
              <div class="grid__col"><a class="objectPreview" href="">
                  <div class="objectPreview__preview"><img class="objectPreview__previewImage" src="{{ url('/') }}/images/content/object.png" alt=""><img class="objectPreview__previewMagnifier" src="{{ url('/') }}/images/general/magnifier.png" alt=""></div>
                  <div class="objectPreview__info objectPreview__info--aqua" href="">
                    <div class="objectPreview__content">
                      <div class="objectPreview__contentTitle">5-к квартира</div>
                      <div class="objectPreview__contentText">Москва, Лобачевского Улица, 92к4 <br>Проспект Вернадского</div>
                      <div class="objectPreview__contentText">15 минут пешком</div>
                    </div>
                    <div class="objectPreview__price"><span class="objectPreview__priceVal">52 000 000 </span><span class="objectPreview__priceLabel">руб.</span></div>
                  </div></a>
              </div>
              <div class="grid__col"><a class="objectPreview" href="">
                  <div class="objectPreview__preview"><img class="objectPreview__previewImage" src="{{ url('/') }}/images/content/object.png" alt=""><img class="objectPreview__previewMagnifier" src="{{ url('/') }}/images/general/magnifier.png" alt=""></div>
                  <div class="objectPreview__info objectPreview__info--yellow" href="">
                    <div class="objectPreview__content">
                      <div class="objectPreview__contentTitle">5-к квартира</div>
                      <div class="objectPreview__contentText">Москва, Лобачевского Улица, 92к4 <br>Проспект Вернадского</div>
                      <div class="objectPreview__contentText">15 минут пешком</div>
                    </div>
                    <div class="objectPreview__price"><span class="objectPreview__priceVal">52 000 000 </span><span class="objectPreview__priceLabel">руб.</span></div>
                  </div></a>
              </div>
            </div>
          </section>
        </div>
      </div>
      <footer class="footer">
        <div class="container">
          <article class="searchRequest footer__searchRequest">
            <h2 class="searchRequest__title">заявка на подбор недвижимости</h2>
            <form class="searchRequest__form" data-id="footer">
              <input type="hidden" name="form" value="Заявка на подбор недвижимости">
              <div class="searchRequest__formCol">
                <input class="customInput searchRequest__formInput" name="name" type="text" placeholder="Ваше имя">
                <input class="customInput searchRequest__formInput" name="phone" type="tel" placeholder="Ваш телефон" required>
                <div class="customCheckbox searchRequest__formCheckbox">
                  <input class="customCheckbox__input" type="radio" id="searchRequest-checkbox-1" name="recallme" checked="">
                  <label class="customCheckbox__label" for="searchRequest-checkbox-1">Перезвонить мне</label>
                </div>
                <div class="customCheckbox searchRequest__formCheckbox">
                  <input class="customCheckbox__input" type="radio" id="searchRequest-checkbox-2" name="sendemail">
                  <label class="customCheckbox__label" for="searchRequest-checkbox-2">Отправить на почту</label>
                </div>
              </div>
              <div class="searchRequest__formCol">
                <textarea class="customTextarea searchRequest__formTextarea" name="text" placeholder="Введите текст Вашего сообщения" maxlength="250"></textarea>
              </div>
              <div class="searchRequest__formBottom">
                <div class="searchRequest__formAlert">Нажимая «Отправить», вы даёте своё согласие на обработку персональных данных.</div>
                <button class="button searchRequest__formButton" data-id="footer">отправить</button>
              </div>
            </form>
          </article>
          <div class="footer__inner">
            <nav class="footer__nav">
              <div class="footer__navItem"><a class="footer__navLink" href="">Главная</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Услуги</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">О компании</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Наш офис</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Купить</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Продать</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Сдать</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Снять</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Обменять</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Советы</a></div>
              <div class="footer__navItem"><a class="footer__navLink" href="">Карта сайта</a></div>
            </nav>
            <div class="footer__content">
              <div class="footer__contentLeft">
                <div class="footer__contentLeftWrapper">
                  <div class="footer__contentLocation">
                    <div class="footer__contentTitle">Московская область:</div>
                    <div class="footer__contentLocationWrapper">
                      <div class="footer__contentLocationCol"><a class="footer__contentLocationLink" href="">Раменское</a><a class="footer__contentLocationLink" href="">Жуковский</a></div>
                      <div class="footer__contentLocationCol"><a class="footer__contentLocationLink" href="">Люберцы</a><a class="footer__contentLocationLink" href="">Воскресенск</a></div>
                    </div>
                  </div>
                  <div class="footer__contentLocation">
                    <div class="footer__contentTitle">Москва:</div>
                    <div class="footer__contentLocationWrapper">
                      <div class="footer__contentLocationCol"><a class="footer__contentLocationLink" href="">Кузьминки</a><a class="footer__contentLocationLink" href="">Рязанский проспект</a><a class="footer__contentLocationLink" href="">Братиславская</a></div>
                      <div class="footer__contentLocationCol"><a class="footer__contentLocationLink" href="">Выхино</a><a class="footer__contentLocationLink" href="">Котельники</a></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="footer__contentRight">
                <div class="footer__contentSocial">
                  <div class="footer__contentSocialWrapper">
                    <div class="footer__contentTitle">мы в соц. сетях:</div>
                    <div class="footer__contentSocialLinks"><a class="footer__contentSocialLink" href=""><img class="footer__contentSocialLinkImg" src="{{ url('/') }}/images/general/icon-inst.png" alt=""></a><a class="footer__contentSocialLink" href=""><img class="footer__contentSocialLinkImg" src="{{ url('/') }}/images/general/icon-vk.png" alt=""></a></div>
                  </div>
                </div>
                <div class="footer__contentAddress">
                  <div class="footer__contentTitle">ОФИС «НЕДВИЖИМОСТЬ - профит» В раменском</div><a class="footer__contentAddressLink" href="">г. Раменское, ул. Вокзальная д. 4, офис 209</a>
                </div>
              </div>
            </div>
            <div class="footer__info">
              <div class="footer__infoItem footer__infoItem--start">
                <div class="footer__copyright">© 1997 - 2017 «ПРОФИТ - Недвижимость»</div>
              </div>
              <div class="footer__infoItem footer__infoItem--center"><a class="footer__policy" href="">Политика хранения и обработки персональных данных</a></div>
              <div class="footer__infoItem footer__infoItem--end"><a class="footer__mail" href="mailto: ramprofit.ru@yandex.ru"><img class="footer__mailIcon" src="{{ url('/') }}/images/general/mail.png" alt="">
                  <div class="footer__mailText">ramprofit.ru@yandex.ru</div></a></div>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <div class="modal" id="modal-1">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <div class="modal__title">Закажите обратный звонок, мы вам перезвоним</div>
          <form class="modalForm">
            <input class="modalForm__input" type="text" placeholder="Ваше имя">
            <input class="modalForm__input" type="tel" placeholder="Ваш телефон" required>
            <button class="button modalForm__button" data-fancybox-thank>Заказать звонок</button>
          </form>
        </div>
      </div>
    </div>
    <div class="modal" id="modal-2">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button><img class="modal__logo" src="{{ url('/') }}/images/general/logo-color.png" alt="">
          <div class="modal__title">Спасибо за заявку!</div>
          <div class="modal__text">Ваши данные были успешно отправлены,<br>мы свяжемся с Вами в рабочее время<br>пн-сб с 10 до 21</div>
        </div>
      </div>
    </div>
    <div class="modal" id="modal-3">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <div class="modal__title">Получите выгодное предложение уже сегодня</div>
          <form class="modalForm">
            <input class="modalForm__input" type="text" placeholder="Ваше имя">
            <input class="modalForm__input" type="tel" placeholder="Ваш телефон" required>
            <button class="button button--blue modalForm__button" data-fancybox-thank>Подобрать недвижимость</button>
          </form>
        </div>
      </div>
    </div>
    <div class="modal" id="modal-4">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <div class="modal__title">Запишитесь на просмотр в любое удобное время</div>
          <form class="modalForm">
            <input class="modalForm__input" type="text" placeholder="Ваше имя">
            <input class="modalForm__input" type="tel" placeholder="Ваш телефон" required>
            <button class="button modalForm__button" data-fancybox-thank>Записаться на просмотр</button>
          </form>
        </div>
      </div>
    </div>
    <div class="modal" id="modal-5">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <div class="modal__title">Закажите звонок специалиста, мы вам перезвоним</div>
          <form class="modalForm">
            <input class="modalForm__input" type="text" placeholder="Ваше имя">
            <input class="modalForm__input" type="tel" placeholder="Ваш телефон" required>
            <button class="button button--blue modalForm__button" data-fancybox-thank>Заказать звонок</button>
          </form>
        </div>
      </div>
    </div>
    <div class="modal" id="modal-6">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <div class="modal__title">оставьте заявку на покупку жилья, мы вам перезвоним</div>
          <form class="modalForm">
            <input class="modalForm__input" type="text" placeholder="Ваше имя">
            <input class="modalForm__input" type="tel" placeholder="Ваш телефон" required>
            <button class="button modalForm__button" data-fancybox-thank>Перейти в каталог</button>
          </form>
        </div>
      </div>
    </div>
    <div class="modal" id="modal-7">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <div class="modal__title">оставьте заявку на продажу жилья, мы вам перезвоним</div>
          <form class="modalForm">
            <input class="modalForm__input" type="text" placeholder="Ваше имя">
            <input class="modalForm__input" type="tel" placeholder="Ваш телефон" required>
            <button class="button modalForm__button" data-fancybox-thank>Оставить заявку</button>
          </form>
        </div>
      </div>
    </div>
    <div class="sideMenu">
      <div class="sideMenu__wrapper">
        <div class="sideMenu__head"><a class="sideMenu__logo" href=""><img class="sideMenu__logoIcon" src="{{ url('/') }}/images/general/logo-color.png" alt=""></a>
          <button class="sideMenu__close"><img class="sideMenu__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
        </div>
        <ul class="sideMenu__userNav">
          <li class="sideMenu__userNavItem"><a class="sideMenu__userNavLink" href="">
              <div class="sideMenu__userNavIcon"><img src="{{ url('/') }}/images/general/employee.png" alt=""></div><span>Вход для сотрудников</span></a></li>
          <li class="sideMenu__userNavItem"><a class="sideMenu__userNavLink" href="">
              <div class="sideMenu__userNavIcon"><img src="{{ url('/') }}/images/general/mail.png" alt=""></div><span>Почта</span></a></li>
          <li class="sideMenu__userNavItem"><a class="sideMenu__userNavLink" href="">
              <div class="sideMenu__userNavIcon"><img src="{{ url('/') }}/images/general/crm.png" alt=""></div><span>CRM</span></a></li>
          <li class="sideMenu__userNavItem"><a class="sideMenu__userNavLink" href="">
              <div class="sideMenu__userNavIcon"><img src="{{ url('/') }}/images/general/user.png" alt=""></div><span>Вход в личный кабинет</span></a></li>
          <li class="sideMenu__userNavItem"><a class="sideMenu__userNavLink" href="">
              <div class="sideMenu__userNavIcon"><img src="{{ url('/') }}/images/general/consultation.png" alt=""></div><span>Онлайн-консультант</span></a></li>
          <li class="sideMenu__userNavItem"><a class="sideMenu__userNavLink" href="">
              <div class="sideMenu__userNavIcon"><img src="{{ url('/') }}/images/general/favorites.png" alt=""></div><span>Избранное</span></a></li>
        </ul>
        <ul class="sideMenu__mainNav">
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon sideMenu__mainNavIcon--white"><img src="{{ url('/') }}/images/general/icon-header-home.png" alt=""></div><span>Главная</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-sale.png" alt=""></div><span>Продать</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-buy.png" alt=""></div><span>Купить</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-rent.png" alt=""></div><span>Сдать</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-to-rent.png" alt=""></div><span>Снять</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-header-location.png" alt=""></div><span>Недвижимость в Московской области</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-country-app.png" alt=""></div><span>Загородная недвижимость</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-green.png" alt=""></div><span>Купить земельный участок</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-header-escort.png" alt=""></div><span>Оформление сделок и юр. сопровождение</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-header-about.png" alt=""></div><span>О компании</span></a></li>
          <li class="sideMenu__mainNavItem"><a class="sideMenu__mainNavLink" href="">
              <div class="sideMenu__mainNavIcon"><img src="{{ url('/') }}/images/general/icon-header-contacts.png" alt=""></div><span>Контакты</span></a></li>
        </ul>
      </div>
    </div>
    <div class="modal modal--wide" id="modalFilter">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <form class="searchForm" action="" style="background-image: url({{ url('/') }}/images/general/bg-search-form.png);">
            <div class="searchForm__head">
              <div class="choiceCheckbox searchForm__choiceCheckbox">
                <input class="choiceCheckbox__input" type="checkbox" id="searchForm-choice-1" name="searchForm-choice">
                <label class="choiceCheckbox__label" for="searchForm-choice-1">раменское</label>
              </div>
              <div class="choiceCheckbox searchForm__choiceCheckbox">
                <input class="choiceCheckbox__input" type="checkbox" id="searchForm-choice-2" name="searchForm-choice">
                <label class="choiceCheckbox__label" for="searchForm-choice-2">жуковский</label>
              </div>
              <div class="choiceCheckbox searchForm__choiceCheckbox">
                <input class="choiceCheckbox__input" type="checkbox" id="searchForm-choice-3" name="searchForm-choice">
                <label class="choiceCheckbox__label" for="searchForm-choice-3">подмосковье</label>
              </div>
              <div class="choiceCheckbox searchForm__choiceCheckbox">
                <input class="choiceCheckbox__input" type="checkbox" id="searchForm-choice-4" name="searchForm-choice">
                <label class="choiceCheckbox__label" for="searchForm-choice-4">москва</label>
              </div>
            </div>
            <div class="searchForm__firstRow">
              <div class="searchForm__firstRowCol">
                <div class="searchForm__title">тип недвижимости</div>
                <div class="customCheckbox searchForm__customCheckbox">
                  <input class="customCheckbox__input" type="radio" id="searchform-checkbox-1" name="searchform-checkbox">
                  <label class="customCheckbox__label" for="searchform-checkbox-1">Вторичное</label>
                </div>
                <div class="customCheckbox searchForm__customCheckbox">
                  <input class="customCheckbox__input" type="radio" id="searchform-checkbox-2" name="searchform-checkbox">
                  <label class="customCheckbox__label" for="searchform-checkbox-2">Новостройка</label>
                </div>
              </div>
              <div class="searchForm__firstRowCol">
                <div class="searchForm__title">общая площадь</div>
                <div class="fromTo">
                  <input class="fromTo__input" type="number" value="0">
                  <div class="fromTo__line"></div>
                  <input class="fromTo__input" type="number" value="0">
                  <div class="fromTo__label">м<sup>2</sup></div>
                </div>
              </div>
              <div class="searchForm__firstRowCol">
                <div class="searchForm__title">цена</div>
                <div class="fromTo">
                  <input class="fromTo__input" type="number" value="0">
                  <div class="fromTo__line"></div>
                  <input class="fromTo__input" type="number" value="0">
                  <div class="fromTo__label">руб.</div>
                </div>
              </div>
              <div class="searchForm__firstRowCol">
                <div class="searchForm__title">тип здания</div>
                <div class="customSelect">
                  <select name="" multiple>
                    <option value="hide">Тип недвижимости</option>
                    <option value="Панельный 1">Панельный 1</option>
                    <option value="Панельный 2">Панельный 2</option>
                    <option value="Панельный 3">Панельный 3</option>
                    <option value="Панельный 4">Панельный 4</option>
                    <option value="Панельный 5">Панельный 5</option>
                    <option value="Панельный 6">Панельный 6</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="searchForm__secondRow">
              <div class="searchForm__secondRowCol">
                <div class="searchForm__title">количество комнат</div>
                <div class="choiceType">
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-1" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-1">Комната</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-2" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-2">1</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-3" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-3">2</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-4" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-4">3</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-5" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-5">4</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-6" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-6">5</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-7" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-7">< 5</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-8" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-8">Доля в квартире</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="searchForm-type-9" name="searchForm-type">
                    <label class="choiceCheckbox__label" for="searchForm-type-9">Студия</label>
                  </div>
                </div>
              </div>
              <div class="searchForm__secondRowCol">
                <div class="searchForm__title">метро</div>
                <div class="customSelect">
                  <select name="" multiple>
                    <option value="hide">Метро</option>
                    <option value="Панельный 2">Метро 2</option>
                    <option value="Панельный 3">Метро 3</option>
                    <option value="Панельный 4">Метро 4</option>
                    <option value="Панельный 5">Метро 5</option>
                    <option value="Панельный 6">Метро 6</option>
                  </select>
                </div>
                <div class="searchForm__metroLink"><a class="formLink" href="">Выбрать на схеме метро</a></div>
              </div>
              <div class="searchForm__secondRowCol">
                <div class="searchForm__title">расстояние до метро</div>
                <div class="fromTo">
                  <div class="fromTo__label">от</div>
                  <input class="fromTo__input" type="number" value="0">
                  <div class="fromTo__label">мин.</div>
                  <div class="customSelect">
                    <select name="">
                      <option value="Панельный 1">Пешком</option>
                      <option value="Панельный 2">Пешком 2</option>
                      <option value="Панельный 3">Пешком 3</option>
                      <option value="Панельный 4">Пешком 4</option>
                      <option value="Панельный 5">Пешком 5</option>
                      <option value="Панельный 6">Пешком 6</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="searchForm__bottom"><a class="formLink searchForm__bottomLink" href="">Очистить условия поиска</a>
              <button class="button searchForm__button">Показать (213)</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="modal modal--wide" id="modalSell">
      <div class="modal__body">
        <div class="modal__wrapper">
          <button class="modal__close" data-fancybox-close><img class="modal__closeIcon" src="{{ url('/') }}/images/general/icon-close-modal.png" alt=""></button>
          <form class="sellForm" action="" style="background-image: url({{ url('/') }}/images/general/bg-search-form.png);">
            <div class="sellForm__row">
              <div class="sellForm__col">
                <div class="sellForm__title">контактное лицо</div>
                <input class="customInput sellForm__input" type="text" placeholder="Ваше Имя">
              </div>
              <div class="sellForm__col">
                <div class="sellForm__title">телефон</div>
                <input class="customInput sellForm__input" type="tel" placeholder="Ваш номер телефона">
              </div>
              <div class="sellForm__col">
                <div class="sellCols">
                  <div class="sellCols__col">
                    <div class="sellForm__title">общая площадь</div>
                    <div class="fromTo">
                      <input class="fromTo__input" type="number" value="0">
                      <div class="fromTo__label">м<sup>2</sup></div>
                    </div>
                  </div>
                  <div class="sellCols__col">
                    <div class="sellForm__title">расположение</div>
                    <div class="customCheckbox sellForm__customCheckbox">
                      <input class="customCheckbox__input" type="radio" id="sellForm-checkbox-1" name="sellForm-checkbox">
                      <label class="customCheckbox__label" for="sellForm-checkbox-1">Москва</label>
                    </div>
                    <div class="customCheckbox sellForm__customCheckbox">
                      <input class="customCheckbox__input" type="radio" id="sellForm-checkbox-2" name="sellForm-checkbox">
                      <label class="customCheckbox__label" for="sellForm-checkbox-2">Подмосковье</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="sellForm__col">
                <div class="sellForm__title">e-mail</div>
                <input class="customInput sellForm__input" type="text" placeholder="Ваш e-mail">
              </div>
              <div class="sellForm__col">
                <div class="sellForm__title">адрес</div>
                <input class="customInput sellForm__input" type="text" placeholder="Адрес">
              </div>
              <div class="sellForm__col">
                <div class="sellForm__title">контактное лицо</div>
                <div class="choiceType">
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-1" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-1">Комната</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-2" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-2">1</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-3" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-3">2</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-4" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-4">3</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-5" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-5">4</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-6" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-6">5</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-7" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-7">< 5</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-8" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-8">Доля в квартире</label>
                  </div>
                  <div class="choiceCheckbox choiceType__choiceCheckbox">
                    <input class="choiceCheckbox__input" type="checkbox" id="sellForm-type-9" name="sellForm-type">
                    <label class="choiceCheckbox__label" for="sellForm-type-9">Студия</label>
                  </div>
                </div>
              </div>
              <div class="sellForm__col">
                <div class="sellForm__title">желаемый офис</div>
                <div class="customSelect">
                  <select name="">
                    <option value="БЕСТ на Проспекте Мира 1">БЕСТ на Проспекте Мира 1</option>
                    <option value="БЕСТ на Проспекте Мира 2">БЕСТ на Проспекте Мира 2</option>
                    <option value="БЕСТ на Проспекте Мира 3">БЕСТ на Проспекте Мира 3</option>
                    <option value="БЕСТ на Проспекте Мира 4">БЕСТ на Проспекте Мира 4</option>
                    <option value="БЕСТ на Проспекте Мира 5">БЕСТ на Проспекте Мира 5</option>
                    <option value="БЕСТ на Проспекте Мира 6">БЕСТ на Проспекте Мира 6</option>
                  </select>
                </div>
              </div>
              <div class="sellForm__col">
                <button class="button button--wide sellForm__button">отправить заявку</button>
              </div>
              <div class="sellForm__col">
                <div class="sellForm__alert">Нажимая «Отправить заявку», вы даёте своё согласие на обработку персональных данных</div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="overlay"></div>
    <script src="{{ mix('js/bundle.min.js') }}"></script>
  </body>
</html>
