@if ($paginator->hasPages())
    <div class="section__bottomCenter">
        <div class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <a class="pagination__item disabled" href="#">Предыдущая</a>
            @else
                <a class="pagination__item" href="{{ $paginator->previousPageUrl() }}">Предыдущая</a>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <a class="pagination__item active" href="#">{{ $page }}</a>
                        @else
                            <a class="pagination__item" href="{{ $url }}">{{ $page }}</a>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <a class="pagination__item" href="{{ $paginator->nextPageUrl() }}">Следующая</a>
            @else
                <a class="pagination__item disabled" href="#">Следующая</a>
            @endif
        </div>
    </div>
@endif
