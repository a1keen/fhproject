<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            DataRowsTableSeeder::class,
            DataTypesTableSeeder::class,
            MenuItemsTableSeeder::class,
            MenusTableSeeder::class,
            PermissionRoleTableSeeder::class,
            PermissionsTableSeeder::class,
            UserPermissionSeeder::class,
            RolesTableSeeder::class,
            SettingsTableSeeder::class,
            TranslationsTableSeeder::class,
            VoyagerDatabaseSeeder::class,
            VoyagerDummyDatabaseSeeder::class
        ]);
        // $this->call(UsersTableSeeder::class);
    }
}
