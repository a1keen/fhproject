<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserPermissionSeeder extends Seeder
{
    /**
     * User role ID
     */
    public $roleID = 2;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissionsIDs = [
            1,  // Browse Admin
            26, // Browse Objects
            27, // Read Objects
            28, // Edit Objects
            29, // Add Objects
            30  // Delete Objects
        ];

        foreach ($permissionsIDs as $k) {

            DB::table('permission_role')->insert([
                'permission_id' => $k,
                'role_id' => $this->roleID
            ]);

        }
    }
}
