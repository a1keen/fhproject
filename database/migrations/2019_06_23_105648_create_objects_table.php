<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('author_id');
            $table->string('type');
            $table->integer('total_price');
            $table->integer('metr_price');
            $table->text('description');
            $table->string('imgages', 5000);
            $table->integer('rooms');
            $table->integer('living_space');
            $table->integer('total_space');
            $table->integer('kitchen_space');
            $table->string('house_material');
            $table->integer('floors');
            $table->integer('floor');
            $table->string('lift');
            $table->string('view_from_window');
            $table->string('balcony');
            $table->string('address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objects');
    }
}
